package archive;

import cz.cvut.eshop.archive.ItemPurchaseArchiveEntry;
import cz.cvut.eshop.archive.PurchasesArchive;
import cz.cvut.eshop.shop.Item;
import cz.cvut.eshop.shop.StandardItem;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class ItemPurchaseArchiveEntryTest {

    private static final int POSITIVE_INCREASE_COUNT = 5;
    private static final int NEGATIVE_INCREASE_COUNT = -5;
    private Item item;
    private ItemPurchaseArchiveEntry archiveEntry;

    @Before
    public void before()
    {
        //Create new archive entry before each test
        item = new StandardItem(1, "name", 3.14f, "category", 12);
        archiveEntry = new ItemPurchaseArchiveEntry(item);
    }

    @Test
    public void testGetRefItem()
    {
        Item actual = archiveEntry.getRefItem();

        assertEquals("Ref item is not the same item, as was passed to constructor.", item, actual);
    }

    @Test
    public void testGetSoldCountInNewArchiveEntry()
    {
        int actual = archiveEntry.getCountHowManyTimesHasBeenSold();

        assertEquals("Sold count in new archive entry has to be 1.",1, actual);
    }

    @Test
    public void testIncreaseCountHowManyTimesHasBeenSold_positiveIncreaseCount()
    {
        testIncreaseCountHowManyTimesHasBeenSold(POSITIVE_INCREASE_COUNT);
    }

    @Test
    public void testIncreaseCountHowManyTimesHasBeenSold_negativeIncreaseCount()
    {
        testIncreaseCountHowManyTimesHasBeenSold(NEGATIVE_INCREASE_COUNT);
    }

    private void testIncreaseCountHowManyTimesHasBeenSold(int increaseCount)
    {
        int before = archiveEntry.getCountHowManyTimesHasBeenSold();

        archiveEntry.increaseCountHowManyTimesHasBeenSold(increaseCount);

        int actual = archiveEntry.getCountHowManyTimesHasBeenSold();

        assertEquals("Different item sold count after increment",before + increaseCount, actual);
    }

}
